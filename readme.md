#Desafio desenvolvido para Webjump

#Instalação
git clone

# Requerimentos
PHP 7 ou mais recente.

# Ferramentas Usadas no desenvolvimento do projeto
- PHP 7.1
- Javascript;
- Jquery;
- Twig 2.0;
- Firebase Storage;
- Firebase DataBase;

#Opcional
- importação de arquivo via CLI: php -f CSVImport.php

#Nota:
A tabela produtos foi deixada somente com 20 produtos para efeito de visualizacao, pois a importacao da tabala import feita por CLI incluirá os demais produtos.


