<?php

declare(strict_types=1);

namespace gugglegum\AbstractEntity\tests;

/**
 * Alternative custom exception class
 *
 * @package gugglegum\AbstractEntity\tests
 */
class CustomException extends \Exception
{

}
