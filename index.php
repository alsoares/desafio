<?php

include_once __DIR__ . '/assets/src/controllers/ProdutoController.php';
require_once __DIR__ . '/assets/src/controllers/CategoriaController.php';

// use Controllers\ProdutoController;
use FastRoute as f;

require 'vendor/autoload.php';

$dispatcher = f\simpleDispatcher(function(f\RouteCollector $r) {
    $r->addRoute(['GET', 'POST'], '/produto[/{id:\d+}]', 'Produto/index');
    $r->addRoute(['GET', 'POST'], '/produto/create', 'Produto/createProduto');
    $r->addRoute(['GET', 'POST'], '/produto/update', 'Produto/updateProduto');
    $r->addRoute(['GET', 'POST'], '/produto/delete', 'Produto/deleteProduto');
    //Route Categoria
    $r->addRoute(['GET', 'POST'], '/categoria', 'Categoria/index');
    $r->addRoute(['GET', 'POST'], '/categoria/create', 'Categoria/createCategoria');
    $r->addRoute(['GET', 'POST'], '/categoria/update', 'Categoria/updateCategoria');
    $r->addRoute(['GET', 'POST'], '/categoria/delete', 'Categoria/deleteCategoria');
    
    // {id} must be a number (\d+)
    //$r->addRoute('GET', '/user/{id:\d+}', 'get_user_handler');
    // The /{title} suffix is optional
    //$r->addRoute('GET', '/articles/{id:\d+}[/{title}]', 'get_article_handler');
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case f\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        break;
    case f\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;
    case f\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];
        list($class, $method) = explode("/", $handler, 2);
        echo call_user_func_array(array(new $class, $method), $vars);
        break;
}