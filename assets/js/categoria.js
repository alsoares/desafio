$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        add: function (e, data) {
            data.context = $('<button/>').text('Upload')
                .appendTo($(".Upload"))
                .click(function () {
                    data.submit();
                });
        },
        done: function (e, data) {
            data.context.text('Upload finished.');
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });

    $(document).on('submit', '#update_categoria', function (event) {
        event.preventDefault();

        var orderData = $(this).serialize();
//        console.log(orderData);
        // return false;

        $.post('/categoria/update', orderData, function (res) {
            $.modal.close();
            location.reload();
        }, 'json');


    });

    $(document).on('submit', '#create_categoria', function (event) {
        event.preventDefault();

        var orderData = $(this).serialize();
//        console.log(orderData);
        // return false;

        $.post('/categoria/create', orderData, function (res) {
            $.modal.close();
            location.reload();
        }, 'json');


    });

    $(document).on('click', 'div.delete > span > a', function (event) {
        event.stopPropagation();
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Confirm!',
            content: 'Simple confirm!',
            buttons: {
                confirm: function () {
//                    console.log($(event.target).data('id'));
                    $.post('/categoria/delete', {id: $(event.target).data('id')}, function(res){
                        $.alert(res);
                        location.reload();
                    });
                },
                cancel: function () {
                    close();
                },

            }
        });
    });

});