$(function () {

    $(document).on('click', '.butt_firebase', function(){
        firebaseUpload();
    });

    $('#fileupload').on('change', function(event){
        readURL(this);
    });

    $(document).on('submit', '#create_produto', function(event){
        event.preventDefault();
        
        var orderData = $(this).serialize();
        // return false;
        
        $.post('/produto/create', orderData, function(res){
        	alert(res);
        	window.location.href = '/produto';
        }, 'json');
    });

    $(document).on('submit', '#update_produto', function (event) {
        event.preventDefault();

        var orderData = $(this).serialize();
//        console.log(orderData);
        

        $.post('/produto/update', orderData, function (res) {
            alert(res);
            window.location.href = '/produto';
        }, 'json');


    });
    
    $(document).on('click', 'div.delete > span > a', function (event) {
        event.stopPropagation();
        $.confirm({
            boxWidth: '30%',
            useBootstrap: false,
            title: 'Confirm!',
            content: 'Simple confirm!',
            buttons: {
                confirm: function () {
//                    console.log($(event.target).data('id'));
                    $.post('/produto/delete', {id: $(event.target).data('id')}, function(res){
                        $.alert(res);
                        location.reload();
                    });
                },
                cancel: function () {
                    close();
                },

            }
        });
    });

});

function firebaseUpload(){
    const ref = firebase.storage().ref();
    const file = $('#fileupload').get(0).files[0];
    const name = (+new Date()) + '-' + file.name;
    const metadata = { contentType: file.type };
    const task = ref.child(name).put(file, metadata);
    task
    .then(snapshot => snapshot.ref.getDownloadURL())
    .then(url => $('#image-address').val(url))
}

function readURL(input) {
    // const input = $('#fileupload').get(0)
    if (input.files && input.files[0]) {
        var reader = new FileReader();            
        reader.onload = function (e) {
            $('#some-image').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}
  