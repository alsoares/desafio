<?php

require_once __DIR__ . '/../../../vendor/autoload.php';

use Coderatio\PhpFirebase\PhpFirebase;
use League\Flysystem\Exception;

class Produto
{
    protected $twig;
    protected $loader;
    protected $pfb;
    protected $data;

    public function __construct()
    {
        $this->loader = new \Twig\Loader\FilesystemLoader('assets/src/views/');
        $this->twig = new \Twig\Environment($this->loader, ['debug' => true]);
        $this->pfb = new PhpFirebase();
//         $this->secretKeyJsonPath = $_SERVER['DOCUMENT_ROOT'] . '/src/secret/desafio-e5a04-firebase-adminsdk-tjh1l-fafdcb242a.json'
        $this->pfb->setTable('produtos');
        $this->data = null;
    }

    /**
     * 
     * Listar Produto Cadastrado
     */
    public function index()
    {
        $path = $_SERVER['REQUEST_URI'];
        
        if ( preg_match('(\d+)', $path, $matches) ){
            $currentPage = $matches[0];
        }else {
            $currentPage = 1;
        }
        
        $this->data = array_filter($this->pfb->getRecords());
        
        $limit = 20; // leaderboard rows limit
        $offset = ($currentPage - 1) * $limit; // offset
        $totalItems = count($this->data); // total items
        $totalPages = ceil($totalItems / $limit);
        $itemsList = array_splice($this->data, $offset, $limit);
        
        $template = $this->twig->load('produto/Produtos.html.twig');
        return $template->render([
            'produtos' => $itemsList,
            'currentPage' => $currentPage,
            'totalPages' => $totalPages,
            'title' => 'Lista de Produtos'
        ]);
    }

    /**
     * 
     * Novo Produto
     */
    public function createProduto()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && (!empty($_POST))) {


            try {
                $this->pfb->insertRecord([
                    'id' => intval('1'),
                    'sku' => $_POST['sku'],
                    'name' => $_POST['name'],
                    'price' => $_POST['price'],
                    'description' => $_POST['description'],
                    'quantity' => $_POST['quantity'],
                    'category' => $_POST['category'],
                    'picture' => $_POST['picture']

                ], true);
                $message = 'Produto Inserido com Sucesso.';
            } catch (Exception $e) {
                $message = 'Erro ' . $e;
            }

            return json_encode($message);
        }

        $this->pfb->setTable('categorias');

        $categorias = array_filter($this->pfb->getRecords());
        unset($categorias[0]);

        $template = $this->twig->load('produto/AddProduto.html.twig');
        return $template->render([
            'title' => 'Cadastro de Produto',
            'action' => 'create_produto',
            'categorias' => $categorias
        ]);
    }
    /**
     * 
     * Atualizar Produto Cadastrado
     */
    public function updateProduto()
    {
        
        if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['id'])) {

            $this->pfb->setTable('categorias');
            $categorias = array_filter($this->pfb->getRecords());

            $this->pfb->setTable('produtos');
            $produto = $this->pfb->getRecord(intval($_GET['id']));

            $template = $this->twig->load('produto/UpdateProduto.html.twig');
            return $template->render([
                'title' => 'Atualizar Categoria',
                'action' => 'update_produto',
                'categorias' => $categorias,
                'produto' => $produto
            ]);
        }


        try {
            $this->pfb->updateRecord(intval($_POST['id']), [
                'sku' => $_POST['sku'],
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'description' => $_POST['description'],
                'quantity' => $_POST['quantity'],
                'category' => $_POST['category'],
                'picture' => $_POST['picture']
            ]);
            $message = 'Produto Atualizado com sucesso';
        } catch (Exception $e) {
            $message = 'Erro' . $e;
        }

        return json_encode($message);
    }

    /**
     * 
     * Apagar Produto Cadastrado
     */
    public function deleteProduto()
    {
        $message = "Desculpe Nenhum Registro foi Apagado";
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['id'])) {
            try {
                $this->pfb->deleteRecord($_POST['id']);
                $message = 'Produto Apagado com Sucesso';
            } catch (Exception $e) {
                $message = 'Erro' . $e;
            }
        
        }
        
        return json_encode($message);
        
        
    }
}
