<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

use Coderatio\PhpFirebase\PhpFirebase;
use gugglegum\AbstractEntity\Exception;

class Categoria
{
    protected $loader;
    protected $twig;
    protected $pfb;

    public function __construct()
    {
        $this->loader = new \Twig\Loader\FilesystemLoader($_SERVER['DOCUMENT_ROOT'] . '/assets/src/views/');
        $this->twig = new \Twig\Environment($this->loader, ['debug' => true]);
        $this->pfb = new PhpFirebase();
        $this->pfb->setTable('categorias');
    }
    /**
     * 
     * Listar todas as Categorias
     */
    public function index()
    {

        $data = $this->pfb->getRecords();
        unset($data[0]);
        // var_dump($data); exit();

        $template = $this->twig->load('categoria/Categories.html.twig');
        return $template->render(['categorias' => $data]);
    }
    /**
     * 
     * Upload arquivo csv Categoria
     */
    public function uploadFile()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && (!empty($_FILES))) {

            $file = $_FILES['files']['name'];
            $array = explode('.', $file);
            $fileName = $array[0];
            $fileExt = $array[1];
            $newName = $fileName . "_" . time() . "." . $fileExt;

            $target = $_SERVER['DOCUMENT_ROOT'] . '/assets/upload/' . $newName;

            move_uploaded_file($_FILES['files']['tmp_name'], $target);

            $file = file_get_contents($target, 'r');
            $list = explode(',', $file);

            $pfb = new PhpFirebase();
            $pfb->setTable('categorias');

            // var_dump($file); exit();

            foreach ($list as $data) {
                $this->response = $this->pfb->insertRecord([
                    'name' => $data
                ], true);
            }
        }
    }
    /**
     * 
     * Importar arquivo csv Categoria
     */
    public function import()
    {
        $categoria = array();
        $row = 0;
        $headers = [];
        $filepath = $_SERVER['DOCUMENT_ROOT'] . "/assets/import.csv";
        if (($handle = fopen($filepath, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                if (++$row == 1) {
                    $headers = array_flip($data); // Get the column names from the header.
                    continue;
                } else {
                    $col1 = $data[$headers['categoria']]; // Read row by the column name.
                    // $col2 = $data[$headers['Col2Name']];
                    // print "Row $row: $col1, $col2\n";
                    // print "Row $row: $col1 \r\n";
                    $cat = explode('|', $col1);
                    if (count($cat) > 0) {
                        foreach ($cat as $key => $val) {
                            if (!in_array($val, $categoria))
                                $categoria[] = $val;
                        }
                    } else {
                        if (!in_array($col1, $categoria))
                            $categoria[] = $col1;
                    }
                }
            }
            fclose($handle);

            // $list = str_replace(';', ',', $categoria);

            // var_dump($categoria); exit();

            $fp = fopen('import' . rand(1, 100) . '.csv', 'w');
            fputcsv($fp, $categoria);
            // foreach ($categoria as $fields) {
            //     fputcsv($fp, $fields);
            // }

            fclose($fp);

            // var_dump($categoria) ;
        }
    }

    /**
     * 
     * Inserir nova Categoria
     */
    public function createCategoria()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['category-name'])) {
            
            try {
                $response = $this->pfb->insertRecord([
                    'name' => $_POST['category-name'],
                  ], true);
                $message = 'Registro Inserido com Sucesso.';
            } catch (Exception $e) {
                $message = 'Erro. Registro não Inserido.';
            }
            
            return json_encode($message);
        }
        
        
        $template = $this->twig->load('categoria/_categoria.html.twig');
        return $template->render([
            'title' => 'Nova Categoria',
            'action' => 'create_categoria',
        ]);
    }

    /**
     * 
     * Atualizar Categoria
     */
    public function updateCategoria()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['id'])) {

            $data = $this->pfb->getRecord(intval($_GET['id']));

            // return var_dump($data);

            $template = $this->twig->load('categoria/_categoria.html.twig');
            return $template->render([
                'title' => 'Atualizar Categoria',
                'action' => 'update_categoria', 
                'categoria' => $data
            ]);
        }

        try {
            $this->pfb->updateRecord($_POST['category-code'], [
                'name' => $_POST['category-name']
            ]);
            $message = 'Registro Atualizado com sucesso';
        } catch (Exception $e) {
            $message = 'Erro' . $e;
        }

        return json_encode($message);
    }

    public function deleteCategoria()
    {

        try {
            $this->pfb->deleteRecord($_POST['id']);
            $message = 'Categoria Apagado com Sucesso';
        } catch (Exception $e) {
            $message = 'Erro' . $e;
        }

        return json_encode($message);
    }
}
