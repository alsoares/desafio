<?php
require_once __DIR__ . '/vendor/autoload.php';

use Coderatio\PhpFirebase\PhpFirebase;

$categoria = array();
$row = 0;
$headers = [];
$filepath = $_SERVER['DOCUMENT_ROOT'] . "assets/import.csv";
if (($handle = fopen($filepath, "r")) !== false) {
    while (($data = fgetcsv($handle, 1000, ";")) !== false) {
        if (++ $row == 1) {
            $headers = array_flip($data); // Get the column names from the header.
//             $categoria[] = implode(',', $data);
            continue;
        } else {
            $col1 = $data[$headers['nome']]; // Read row by the column name.
            $col2 = $data[$headers['sku']]; // $col2 = $data[$headers['Col2Name']];
            $col3 = $data[$headers['descricao']]; // print "Row $row: $col1, $col2\n";
            $col4 = $data[$headers['quantidade']];
            $col5 = $data[$headers['preco']];
            $col6 = $data[$headers['categoria']];
            // print "Row $row: $col1 \r\n";
            // $cat = explode('|', $col6);
            // if (count($cat) > 0) {
            // foreach ($cat as $key => $val) {
            // if (! in_array($val, $categoria))
            // $categoria[] = $val;
            // }
            // } else {
            // if (! in_array($col1, $categoria))
            // $categoria[] = $col6;
            // }
            
            $categoria[$row]['name'] = $col1;
            $categoria[$row]['sku'] = $col2;
            $categoria[$row]['descricao'] = $col3;
            $categoria[$row]['quantidade'] = $col4;
            $categoria[$row]['preco'] = $col5;
            $categoria[$row]['categoria'] = $col6;
        }
        
    }
    
    fclose($handle);
    
    $pfb = new PhpFirebase();
    $pfb->setTable('produtos');
    
    // var_dump($file); exit();
    try {
        foreach ($categoria as $data) {
            $response = $pfb->insertRecord([
                'id' => 1,
                'name' => $data['name'],
                'sku' => $data['sku'],
                'description' => $data['descricao'],
                'quantity' => $data['quantidade'],
                'price' => $data['preco'],
                'category' => explode('!', $data['categoria']),
                'picture' => ''
            ], true);

//             var_dump($data['name']);
            
            $mensagem = "Arquivo Importado para a Base de Dados com Sucesso";
        }
        
    } catch (Exception $e) {
            $mensagem = 'Erro ao Importar o Arquivo de Dados ' . $e;
    }

echo $mensagem; die;


